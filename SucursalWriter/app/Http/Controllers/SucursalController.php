<?php

namespace App\Http\Controllers;

use App\Jobs\SaveJob;
use App\Jobs\UpdateJob;
use App\Jobs\DeleteJob;
use App\Models\Sucursal;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SucursalController extends Controller
{
    public function store(Request $request){
        $sucursal = Sucursal::create($request->only('nombre', 'administrador', 'telefono', 'direccion', 'fax', 'pedidos' ));
        
        SaveJob::dispatch($sucursal->toArray());
        
        return response($sucursal, Response::HTTP_CREATED);
    }
    public function update($id, Request $request){
        $sucursal = Sucursal:: find($id);

        $sucursal->update($request->only('nombre', 'administrador', 'telefono', 'direccion', 'fax', 'pedidos' ));

        UpdateJob::dispatch($sucursal->toArray());

        return response($sucursal, Response::HTTP_ACCEPTED);
    }

    public function destroy ($id){
        Sucursal::destroy($id);

        DeleteJob::dispatch($id);

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
