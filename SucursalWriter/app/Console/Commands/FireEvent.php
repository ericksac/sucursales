<?php

namespace App\Console\Commands;

use App\Jobs\SaveJob;
use Illuminate\Console\Command;

class FireEvent extends Command
{
    protected $signature = 'fire';


    public function handle()
    {
        SaveJob::dispatch();
    }
}
