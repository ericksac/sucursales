<?php

use App\Http\Controllers\SucursalController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/sucursales', [SucursalController::class, 'store' ]);
Route::put('/sucursales/{id}', [SucursalController::class, 'update' ]);
Route::delete('/sucursales/{id}', [SucursalController::class, 'destroy' ]);
