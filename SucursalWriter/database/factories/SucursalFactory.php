<?php

namespace Database\Factories;

use App\Models\Sucursal;
use Illuminate\Database\Eloquent\Factories\Factory;

class SucursalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sucursal::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre'=> $this->faker->unique()->sentence(),
            'administrador'=> $this->faker->name(),
            'telefono'=> $this->faker->phoneNumber(),
            'direccion'=> $this->faker->address(),
            'fax'=> $this->faker->e164PhoneNumber()
        ];
    }
}
