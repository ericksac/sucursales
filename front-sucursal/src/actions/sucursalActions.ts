import {
    AGREGAR_SUCURSAL_ERROR,
    AGREGAR_SUCURSAL,
    AGREGAR_SUCURSAL_EXITO,
    COMENZAR_DESCARGA_SUCURSALES,
    DESCARGA_SUCURSALES_EXITO,
    DESCARGA_SUCURSALES_ERROR,
    SUCURSAL_ELIMINADO_ERROR,
    SUCURSAL_ELIMINADO_EXITO,
    OBTENER_SUCURSAL_ELIMINAR,
    SUCURSAL_EDITADO_ERROR,
    SUCURSAL_EDITADO_EXITO,
    OBTENER_SUCURSAL_EDITAR,
    COMENZAR_EDICION_SUCURSAL
} from '../types'

import clienteAxios, {clienteAxios2} from '../config/axios';
import Swal from 'sweetalert2';

export function obtenerSucursalesAction (){
    return async (dispatch: any)=>{
        dispatch(descargarSucursales());
        try {
            const respuesta = await clienteAxios2.get('/sucursales');
            dispatch(descargarSucursalesExitosa(respuesta.data));
        } catch (error) {
            console.log(error);
            dispatch(descargarSucursalesError())
        }

    }
}

const descargarSucursales = () =>({
    type: COMENZAR_DESCARGA_SUCURSALES,
    payload: true
});

const descargarSucursalesExitosa = (sucursales:[]) =>({
    type:   DESCARGA_SUCURSALES_EXITO,
    payload: sucursales
});

const descargarSucursalesError = () =>({
    type: DESCARGA_SUCURSALES_ERROR,
    payload: true 
});

export function crearNuevaSucursalAction(sucursal: SucursalType ){
    return async (dispatch: any) =>{
        dispatch(agregarSucursal());
        try {
            await clienteAxios.post('/sucursales', sucursal);

            dispatch( agregarSucursalExito(sucursal) )
            Swal.fire(
                'Correcto',
                'La sucursal se agregó correctamente',
                'success'
            )

        } catch (error) {
            dispatch(agregarSucursalError(true))

            //Alerta de error
            Swal.fire({
                icon: 'error',
                title: 'Hubo un error',
                text: 'Hubo un error, intenta de nuevo'
            });
        }
    }
}

const agregarSucursal =()=> ({
    type: AGREGAR_SUCURSAL
})

const agregarSucursalExito = (sucursal:SucursalType) =>({
    type: AGREGAR_SUCURSAL_EXITO,
    payload: sucursal
})

const agregarSucursalError= (estado: boolean) =>({
    type: AGREGAR_SUCURSAL_ERROR,
    payload: estado
});

export function obtenerSucursalEditar (sucursal: SucursalType){
    return (dispatch:any) =>{
        dispatch(obtenerSucursalEditarAction(sucursal))
    }
}

const obtenerSucursalEditarAction = (sucursal:SucursalType) =>({
    type: OBTENER_SUCURSAL_EDITAR,
    payload: sucursal
})

export function editarSucursalAction (sucursal: SucursalType) {
    return async (dispatch:any) =>{
        dispatch(editarSucursal());
        try {
            await clienteAxios.put(`/sucursales/${sucursal.id}`, sucursal);

            dispatch(editarSucursalExito(sucursal));
            Swal.fire(
                'Editado',
                'La sucursal se editó correctamente',
                'success'
            )
        } catch (error) {
            dispatch(editarSucursalError());
        }
    }
}

const editarSucursal = () =>({
    type: COMENZAR_EDICION_SUCURSAL
})

const editarSucursalExito = (sucursal: SucursalType) =>({
    type: SUCURSAL_EDITADO_EXITO,
    payload: sucursal
})

const editarSucursalError = () =>({
    type: SUCURSAL_EDITADO_ERROR,
    payload: true
})

export function borrarSucursalAction (id:number) {
    return async (dispatch:any) =>{
        dispatch (obtenerSucursalEliminar(id));
        try {
            await clienteAxios.delete(`/sucursales/${id}`);
            dispatch(eliminarSucursalExito());

            Swal.fire(
                'Eliminada!',
                'La sucursal se eliminó correctamente.',
                'success'
              )
        } catch (error) {
            dispatch(eliminarSucursalError());
        }
    }
}

const obtenerSucursalEliminar = (id:number) =>({
    type: OBTENER_SUCURSAL_ELIMINAR,
    payload: id
})

const eliminarSucursalExito = ()=> ({
    type: SUCURSAL_ELIMINADO_EXITO
});

const eliminarSucursalError = () => ({
    type: SUCURSAL_ELIMINADO_ERROR,
    payload: true
});