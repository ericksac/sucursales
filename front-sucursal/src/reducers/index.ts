import { combineReducers } from 'redux'
import sucursalesReducer from './sucursalesReducer'
import alertaReducer  from './alertaReducer'
const rootReducer =  combineReducers({
    sucursales : sucursalesReducer,
    alerta: alertaReducer
})

export type IRootState = ReturnType<typeof rootReducer>; 

export default rootReducer;