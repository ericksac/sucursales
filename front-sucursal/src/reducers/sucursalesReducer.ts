import {
    AGREGAR_SUCURSAL_ERROR,
    AGREGAR_SUCURSAL,
    AGREGAR_SUCURSAL_EXITO,
    COMENZAR_DESCARGA_SUCURSALES,
    DESCARGA_SUCURSALES_EXITO,
    DESCARGA_SUCURSALES_ERROR,
    SUCURSAL_ELIMINADO_ERROR,
    SUCURSAL_ELIMINADO_EXITO,
    OBTENER_SUCURSAL_ELIMINAR,
    SUCURSAL_EDITADO_ERROR,
    SUCURSAL_EDITADO_EXITO,
    OBTENER_SUCURSAL_EDITAR
} from '../types'

//Cada reducer tiene su propio state
const initialState: SucursalState = {
    sucursales: [],
    error: null,
    cargando: false,
    sucursaleliminar: null,
    sucursaleditar: null
}

// eslint-disable-next-line import/no-anonymous-default-export
export default (state: SucursalState = initialState, action: SucursalAction) => {
    switch (action.type) {
        case COMENZAR_DESCARGA_SUCURSALES:
        case AGREGAR_SUCURSAL:
        case SUCURSAL_ELIMINADO_ERROR:
        case SUCURSAL_EDITADO_ERROR:
            return {
                ...state,
                loading: true
            }
        case AGREGAR_SUCURSAL_EXITO:
            return {
                ...state,
                loading: false,
                sucursales: [...state.sucursales, action.payload]
            }
        case AGREGAR_SUCURSAL_ERROR:
        case DESCARGA_SUCURSALES_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case DESCARGA_SUCURSALES_EXITO:
            return {
                ...state,
                loading: false,
                error: null,
                sucursales: action.payload
            }
        case OBTENER_SUCURSAL_ELIMINAR:
            return {
                ...state,
                sucursaleliminar: action.payload
            }
        case SUCURSAL_ELIMINADO_EXITO:
            return {
                ...state,
                sucursales: state.sucursales.filter(
                    sucursal => sucursal.id!== state.sucursaleliminar),
                sucursaleliminar: null
            }
        case OBTENER_SUCURSAL_EDITAR:
            return {
                ...state,
                sucursaleditar: action.payload
            }
        case SUCURSAL_EDITADO_EXITO:
            return {
                ...state,
                sucursaleditar: null,
                sucursales: state.sucursales.map(sucursal =>
                    sucursal.id === action.payload.id ? sucursal = action.payload :
                        sucursal
                )
            }
        default:
            return state;
    }
}