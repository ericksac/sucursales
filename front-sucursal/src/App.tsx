import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import store from './store';
import Header from './components/Header';
import Sucursales from './components/Sucursales'
import NuevaSucursal from './components/NuevaSucursal';
import EditarSucursal from './components/EditarSucursal';

function App() {
  return (
    <Router>
      <Provider store= {store}>
        <Header/>
        <div className= "">
        <Switch>
          <Route exact path="/" component= {Sucursales}/>
           <Route exact path="/sucursales/nuevo" component= {NuevaSucursal}/>
          <Route exact path="/sucursales/editar/:id" component= {EditarSucursal}/>
        </Switch>
      </div>
      </Provider>
    </Router>
  );
}

export default App;
