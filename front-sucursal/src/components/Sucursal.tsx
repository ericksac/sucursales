import React from 'react';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

import { useDispatch } from 'react-redux'; 
import { borrarSucursalAction,obtenerSucursalEditar } from '../actions/sucursalActions';

const Sucursal= (sucursal: any) =>{
    const {id} = sucursal.sucursal;
    const dispatch = useDispatch();
    const history = useHistory();
    const confirmarEliminarFiscalia = (id:number) =>{
       
        //Preguntar al usuario
        Swal.fire({
            title: '¿Estás seguro(a)?',
            text: "Una Sucursal que se elimina no se puede recuperar",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, eliminar!',
            cancelButtonText: 'Cancelar'
          }).then((result) => {
            if (result.isConfirmed) {
                
            dispatch(borrarSucursalAction(id));
              
            }
          })
    } 

    const redireccionarEdicion = (suc:SucursalType) =>{
        dispatch(obtenerSucursalEditar(suc.sucursal));
        history.push(`/sucursales/editar/${suc.sucursal.id}`);
    }

    return (
            <div className="align-middle">
                <button 
                    type="button"
                    onClick= { () => redireccionarEdicion(sucursal)} 
                    className="btn btn-warning"
                    >Editar</button>
                <button
                    type="button"
                    className="btn btn-danger ms-2"
                    onClick={ () => confirmarEliminarFiscalia (id)}    
                >Eliminar</button>
            </div>
        
    );
}

export default  Sucursal;