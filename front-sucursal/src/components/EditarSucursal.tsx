import React, { useState, useEffect, ChangeEventHandler } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { useHistory } from "react-router-dom";

import { editarSucursalAction } from '../actions/sucursalActions';
//import { mostrarAlerta, ocultarAlertaAction } from '../actions/alertaActions';
//import Error from './Error';
import { IRootState } from '../reducers/index';

type FormElement = React.FormEvent<HTMLFormElement>;
type Change = React.ChangeEvent<HTMLFormElement>;

const EditarSucursal: React.FunctionComponent<RouteComponentProps> = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    //State del componente
    const [sucursal, guardarSucursal] = useState({
        nombre: "",
        direccion: "",
        telefono: "",
        administrador: "",
        fax: "",
        pedidos: 0
    });

    const sucursalEditar = useSelector((state: IRootState) => state.sucursales.sucursaleditar);

    useEffect(() => {
        guardarSucursal(sucursalEditar);
    }, [sucursalEditar]);

    const cargando = useSelector((state: IRootState) => state.sucursales.cargando)
    const error = useSelector((state: IRootState) => state.sucursales.error)
    const alerta = useSelector((state: IRootState) => state.alerta.alerta)

    const onChangeFormulario: ChangeEventHandler = (e: Change) => {
        guardarSucursal({
            ...sucursal,
            [e.target.name]: e.target.value,
        });
    };

    const { nombre, direccion, telefono, administrador, fax, pedidos } = sucursal;


    const submitEditarSucursal = (e: FormElement) => {
        e.preventDefault();

        dispatch(editarSucursalAction(sucursal));
        history.push("/");
    };

    return (
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <h2 className="text-center text-success fw-bold pt-3">
                        Editar Sucursal
                    </h2>
                    <div className="">
                        {error ? <div className="alert alert-primary" role="alert">
                            Hubo un error al guardar la fiscalía.
                        </div> : null}
                        <form
                            onSubmit={submitEditarSucursal}
                            className=" m-0 p-5  shadow "
                        >
                            <div className="form-group">
                                <label
                                    className="pb-2 fw-bold"
                                > Nombre de Sucursal </label>
                                <input type="text"
                                    className="form-control"
                                    placeholder="Nombre "
                                    name="nombre"
                                    value={nombre}
                                    onChange={onChangeFormulario}
                                />
                            </div>
                            <div className="form-group">
                                <label
                                    className="pb-2 pt-2 fw-bold"
                                > Administrador </label>
                                <input type="text"
                                    className="p-2 form-control"
                                    placeholder="Administrador"
                                    name="administrador"
                                    value={administrador}
                                    onChange={onChangeFormulario}

                                />
                            </div>
                            <div className="form-group">
                                <label
                                    className="pb-2 pt-2 fw-bold"
                                > Teléfono </label>
                                <input type="text"
                                    className="p-2 form-control"
                                    placeholder="Teléfono"
                                    name="telefono"
                                    value={telefono}
                                    onChange={onChangeFormulario}
                                />
                            </div>
                            <div className="form-group">
                                <label
                                    className="pb-2 pt-2 fw-bold"
                                > Dirección </label>
                                <input type="text"
                                    className="p-2 form-control"
                                    placeholder="Dirección"
                                    name="direccion"
                                    value={direccion}
                                    onChange={onChangeFormulario}
                                />
                            </div>
                            <div className="form-group">
                                <label
                                    className="pb-2 pt-2 fw-bold"
                                > Fax </label>
                                <input type="text"
                                    className="p-2 form-control"
                                    placeholder="Fax"
                                    name="fax"
                                    value={fax}
                                    onChange={onChangeFormulario}
                                />
                            </div>
                            <div className="form-group">
                                <label
                                    className="pb-2 pt-2 fw-bold"
                                > Pedidos </label>
                                <input type="number"
                                    className="p-2 form-control"
                                    placeholder="Pedidos"
                                    name="pedidos"
                                    value={pedidos}
                                    onChange={onChangeFormulario}
                                />
                            </div>
                            {alerta ? <div className="alert alert-primary" role="alert">
                                {alerta.msg}
                            </div> : null}
                            <div className="d-grid gap-2 mt-3">
                                <button className="btn btn-success btn-lg" type="submit">Editar</button>
                            </div>
                        </form>
                        {cargando ? <p>Cargando...</p> : null}
                    </div>
                </div>
            </div>
        </div>
    )
}
export default EditarSucursal;