import React from 'react';
import { Link } from 'react-router-dom';

const Header: any = () => {
    return (
        <nav className="navbar navbar-dark bg-dark bg-gradient"
        >
            <div className="container-fluid">
                <div className="navbar-brand">
                    <h1>
                        <Link to={'/'}
                            className="fw-bold text-light text-decoration-none"
                        >
                            Sucursales XYZ
                        </Link>
                    </h1>
                </div>
                <Link to="/sucursales/nuevo"
                    className="btn btn-primary"
                > &#43; Agregar Sucursal  </Link>
            </div>
        </nav>
    );

}

export default Header;