import React, { Fragment, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { useGlobalFilter, useAsyncDebounce, useTable, usePagination } from 'react-table';

import { IRootState } from '../reducers/index';

import { obtenerSucursalesAction } from "../actions/sucursalActions";
import moment from 'moment'
import Sucursal from "./Sucursal";
type FilterGlobal = {
  preGlobalFilteredRows: any,
  globalFilter: any,
  setGlobalFilter: any
}

function GlobalFilter(filter: FilterGlobal) {

  const {
    preGlobalFilteredRows,
    globalFilter,
    setGlobalFilter } = filter;
  const count = preGlobalFilteredRows.length;
  const [value, setValue] = React.useState(globalFilter);
  const onChange = useAsyncDebounce((value) => {
    setGlobalFilter(value || undefined);
  }, 200);

  return (

    <div className="input-group">
      <div className="input-group-prepend">
        <span className="input-group-text" id="basic-addon1">
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="24" fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
            <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"></path>
          </svg>
        </span>
      </div>
      <input data-cy="filtro-input" value={value || ""} type="text" className="form-control" placeholder={`${count} sucursales...`} aria-label="Username" aria-describedby="basic-addon1"
        onChange={(e) => {
          setValue(e.target.value);
          onChange(e.target.value);
        }} />
    </div>
  );
}

type cel=  { cell: {
   value :any
  } 
}
type dataRow = {
    row:any
}
const Sucursales = () => {
  const dispatch = useDispatch();

  //obtener el state
  const sucursales = useSelector((state: IRootState) => state.sucursales.sucursales);

  useEffect(() => {
    const cargarSucursales = () => dispatch(obtenerSucursalesAction());
    cargarSucursales();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const error = useSelector((state: IRootState) => state.sucursales.error);
  const cargando = useSelector((state: IRootState) => state.sucursales.cargando);


  const data = sucursales;
  
  const columns = React.useMemo(
    () => [
      {
        Header: "Listado",
        columns: [
          {
            Header: 'Id',
            accessor: "id"
          },
          {
            Header: 'Nombre',
            accessor: 'nombre'
          },
          {
            Header: 'Administrador',
            accessor: 'administrador'
          },
          {
            Header: 'Teléfono',
            accessor: 'telefono'
          },
          {
            Header: 'Dirección',
            accessor: 'direccion'
          },
          {
            Header: 'Fax',
            accessor: 'fax'
          },
          {
            Header: 'Pedidos',
            accessor: 'pedidos'
          },
          {
            Header: 'Actualizado',
            accessor: 'created_at',
            Cell: (varCel: cel) => {
              const {value} = varCel.cell;
              return moment(value).format("DD-MM-YYYY hh:mm:ss")
            }
          },
          {
            Header: 'Creado',
            accessor: 'updated_at',
            Cell: (varCel: cel) => {
              const {value} = varCel.cell;
              return moment(value).format("DD-MM-YYYY hh:mm:ss")
            }
          },
          {
              Header:'Acciones',
              accesor:'',
              Cell: (Row: dataRow) => {
                return <Sucursal sucursal={Row.row.original} />
              }
          }
        ]
      }
    ],
    []
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    
    prepareRow,
    state,
    preGlobalFilteredRows,
    setGlobalFilter,
    page,
    canPreviousPage,
    canNextPage,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize }
  } = useTable({
    columns,
    data,
    initialState:{pageIndex: 0},
  },
    useGlobalFilter,
    usePagination
  );


  return (
    <Fragment>
      <h2 className="text-center text-success fw-bold pt-3">
        Listado de Sucursales
      </h2>
      {error ? (
       		<div className="alert alert-primary" role="alert">
           Hubo un error al cargar las fiscalías.
         </div>
      ) : null}
      {cargando ? <p className="text-center">Cargando ... </p> : null} 

      <div className="d-flex justify-content-center mt-4 ms-5 me-5">
        <GlobalFilter
          preGlobalFilteredRows={preGlobalFilteredRows}
          globalFilter={state.globalFilter}
          setGlobalFilter={setGlobalFilter}
        />
      </div>
      <div className="d-flex justify-content-center mt-1 ms-5 me-5">
        <table
          className="table table-striped table-bordered table-primary table-hover"
          {...getTableProps()}
          style={{ borderCollapse: "collapse", width: "100%" }}
        >
          <thead className="table-dark bg-gradient text-center">
            {headerGroups.map((group) => (
              <tr {...group.getHeaderGroupProps()}>
                {group.headers.map((column) => (
                  <th {...column.getHeaderProps()}>{column.render("Header")}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>

            {page.map((row, i) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return <td {...cell.getCellProps()}>{cell.render("Cell")}</td>;
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <div className="pagination d-flex justify-content-center mt-4 ms-5 me-5">
        <button  className="btn btn-info ms-1 btn-sm" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
          {"<<"}
        </button>{" "}
        <button className="btn btn-info ms-1 btn-sm" onClick={() => previousPage()} disabled={!canPreviousPage}>
          {"<"}
        </button>{" "}
        <button className="btn btn-info ms-1 btn-sm" onClick={() => nextPage()} disabled={!canNextPage}>
          {">"}
        </button>{" "}
        <button className="btn btn-info ms-1 btn-sm" onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
          {">>"}
        </button>{" "}
        <span>
          Página{" "}
          <strong>
            {pageIndex + 1} de {pageCount}
          </strong>{" "}
        </span>
        <span>
          | Ir a la página:{" "}
          <input
            type="number"
            defaultValue={pageIndex + 1}
            onChange={(e) => {
              const page = e.target.value ? Number(e.target.value) - 1 : 0;
              gotoPage(page);
            }}
            style={{ width: "100px" }}
          />
        </span>{" "}
        <select
          value={pageSize}
          onChange={(e) => {
            setPageSize(Number(e.target.value));
          }}
        >
          {[2, 10, 20, 30, 40, 50].map((pageSize) => (
            <option key={pageSize} value={pageSize}>
              Mostrar {pageSize}
            </option>
          ))}
        </select>
      </div>
    </Fragment>
  );
};

export default Sucursales;