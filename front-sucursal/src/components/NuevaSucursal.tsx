import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';

import { crearNuevaSucursalAction } from '../actions/sucursalActions';
import { mostrarAlerta, ocultarAlertaAction } from '../actions/alertaActions';
import { IRootState } from '../reducers/index';

type FormElement = React.FormEvent<HTMLFormElement>;

const NuevaSucursal: React.FunctionComponent<RouteComponentProps> = ({ history }) => {

    //State del componente
    const [nombre, guardarNombre] = useState('');
    const [administrador, guardarAdministrador] = useState('');
    const [telefono, guardarTelefono] = useState('');
    const [direccion, guardarDireccion] = useState('');
    const [fax, guardarFax] = useState('');
    const [pedidos, guardarPedidos] = useState('0');

    const dispatch = useDispatch();

    const cargando = useSelector((state: IRootState) => state.sucursales.cargando)
    const error = useSelector((state: IRootState) => state.sucursales.error)
    const alerta = useSelector((state: IRootState) => state.alerta.alerta)

    const agregarSucursal = (sucursal:SucursalType) => dispatch(crearNuevaSucursalAction( sucursal ));

    const submitNuevaSucursal = async (e: FormElement) => {
        e.preventDefault();

        if(nombre.trim() === '' || direccion.trim() === ''|| telefono.trim() === ''
        || administrador.trim() === '' || fax.trim() === ''
        ){

            const alerta ={
                msg: 'Todos los campos son obligatorios, excepto los pedidos',
                classes: ''
            }
            dispatch (mostrarAlerta(alerta));
            console.log("error")
            return;
        }
        dispatch(ocultarAlertaAction());

        await agregarSucursal({
            nombre,
            administrador,
            telefono,
            direccion,
            fax,
            pedidos
        });

        history.push('/');
    }

    return (
        <div className="row justify-content-center">
            <div className="col-md-8">
                <div className="card">
                    <h2 className="text-center text-primary fw-bold pt-3">
                        Agregar Nueva Sucursal
                    </h2>
                    <div className="">
                      { error ? <div className="alert alert-danger" role="alert">
  Hubo un error al guardar la sucursal.
</div> : null } 
                        <form
                            onSubmit={submitNuevaSucursal}
                            className=" m-0 p-5  shadow "
                        >
                            <div className="form-group">
                                <label
                                    className="pb-2 fw-bold"
                                > Nombre de Sucursal </label>
                                <input type="text"
                                    className="form-control"
                                    placeholder="Nombre "
                                    name="nombre"
                                    value={nombre}
                                    onChange={e => guardarNombre(e.target.value)}
                                />
                            </div>
                            <div className="form-group">
                                <label
                                    className="pb-2 pt-2 fw-bold"
                                > Administrador </label>
                                <input type="text"
                                    className="p-2 form-control"
                                    placeholder="Administrador"
                                    name="administrador"
                                    value={administrador}
                                    onChange={e => guardarAdministrador(e.target.value)}

                                />
                            </div>
                            <div className="form-group">
                                <label
                                    className="pb-2 pt-2 fw-bold"
                                > Teléfono </label>
                                <input type="text"
                                    className="p-2 form-control"
                                    placeholder="Teléfono"
                                    name="telefono"
                                    value={telefono}
                                    onChange={e => guardarTelefono(e.target.value)}
                                />
                            </div>
                            <div className="form-group">
                                <label
                                    className="pb-2 pt-2 fw-bold"
                                > Dirección </label>
                                <input type="text"
                                    className="p-2 form-control"
                                    placeholder="Dirección"
                                    name="direccion"
                                    value={direccion}
                                    onChange={e => guardarDireccion(e.target.value)}
                                />
                            </div>
                            <div className="form-group">
                                <label
                                    className="pb-2 pt-2 fw-bold"
                                > Fax </label>
                                <input type="text"
                                    className="p-2 form-control"
                                    placeholder="Fax"
                                    name="fax"
                                    value={fax}
                                    onChange={e => guardarFax(e.target.value)}
                                />
                            </div>
                            <div className="form-group">
                                <label
                                    className="pb-2 pt-2 fw-bold"
                                > Pedidos </label>
                                <input type="number"
                                    className="p-2 form-control"
                                    placeholder="Pedidos"
                                    name="pedidos"
                                    value={pedidos}
                                    onChange={e => guardarPedidos(e.target.value)}
                                />
                            </div>

                             {alerta ? <div className="alert alert-danger" role="alert">
  {alerta.msg}
</div>: null } 
                            <div className="d-grid gap-2 mt-3">
                                <button className="btn btn-primary btn-lg" type="submit">+ Agregar</button>
                            </div>
                        </form>
                        {cargando ? <p>Cargando...</p> : null}
                    </div>
                </div>
            </div>
        </div>
    )
}
export default NuevaSucursal;