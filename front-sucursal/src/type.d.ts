interface ISucursal {
    id:number
    nombre:string
    administrador:string
    telefono:string
    direccion:string
    fax:string
    numero_pedidos:number
    fecha_creacion:string
    fecha_modificacion:string
}

type SucursalType =  ReturnType<typeof ISucursal>;

type SucursalState = {
    sucursales: ISucursal[],
    error: any,
    cargando: boolean,
    sucursaleliminar: any,
    sucursaleditar: any
}
  
type SucursalAction = {
    type: string
    payload: any
}