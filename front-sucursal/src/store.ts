import { createStore, applyMiddleware, compose, Store } from 'redux'
import thunk from 'redux-thunk';
import reducer from './reducers';

import { composeWithDevTools } from 'redux-devtools-extension';

const store:Store = createStore(
    reducer,
    compose( applyMiddleware(thunk),
    composeWithDevTools()
    )
);

export default store;