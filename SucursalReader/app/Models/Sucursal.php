<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloq;

class Sucursal extends Eloq
{
   protected $connection = 'mongodb';
   protected $collection = 'sucursales';

   protected $guarded = [];
}