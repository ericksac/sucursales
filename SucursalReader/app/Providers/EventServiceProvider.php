<?php

namespace App\Providers;

use App\Jobs\SaveJob;
use App\Jobs\UpdateJob;
use App\Jobs\DeleteJob;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;


class EventServiceProvider extends ServiceProvider
{
    public function boot()
    {
        \App::bindMethod(SaveJob::class.'@handle', fn($job)=> $job->handle() );
        \App::bindMethod(UpdateJob::class.'@handle', fn($job)=> $job->handle() );
        \App::bindMethod(DeleteJob::class.'@handle', fn($job)=> $job->handle() );
    }
}
