<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sucursal;

use Symfony\Component\HttpFoundation\Response;
class SucursalController extends Controller
{

    public function index(){
        return Sucursal::all();
    } 

    public function show($id){
        $res = Sucursal::where('id', (int)$id)->get();
        if(count($res)>0){
            return $res[0];
        }else{
            return [];
        }
    }
}
