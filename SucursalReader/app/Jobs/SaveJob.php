<?php

namespace App\Jobs;

use App\Models\Sucursal;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SaveJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

        /**
     * Create a new job instance.
     *
     * @return void
     */

    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function handle()
    {
        $sucursal = new Sucursal();
        $sucursal->id= $this->data['id'];
        $sucursal->nombre= $this->data['nombre'];
        $sucursal->administrador= $this->data['administrador'];
        $sucursal->telefono = $this->data['telefono'];
        $sucursal->direccion= $this->data['direccion'];
        $sucursal->fax= $this->data['fax'];
        $sucursal->pedidos= $this->data['pedidos'];
        $sucursal->save();
 
    }
}
