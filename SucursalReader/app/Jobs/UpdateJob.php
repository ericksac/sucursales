<?php

namespace App\Jobs;

use App\Models\Sucursal;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $id =$this->data['id'];
        $sucur = Sucursal::where('id', (int)$id)->get();
        $sucursal = Sucursal::find($sucur[0]->_id);
        $sucursal->nombre= $this->data['nombre'];
        $sucursal->administrador= $this->data['administrador'];
        $sucursal->telefono = $this->data['telefono'];
        $sucursal->direccion= $this->data['direccion'];
        $sucursal->fax= $this->data['fax'];
        $sucursal->pedidos= $this->data['pedidos'];
        $sucursal->save();

    }
}
